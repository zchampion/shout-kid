"""
Saves the current conversation id to file so that the KID can find it later.
"""
import toybox
import logging

logger = logging.getLogger('kid.save_id')


def save_id(file=None):
    ast = toybox.Astronaut()
    scw = toybox.ScreenWriter(file=file)

    ast.launch()

    key_conversation = ast.get_current_conversation_title()
    verification_prompt = f'Is {key_conversation[0]} (ID: {key_conversation[1]}) ' \
                          f'the correct conversation to save? [y/n]  '

    if input(verification_prompt).lower() == 'y':
        logger.debug(f'Saving {key_conversation[1]}')
        scw.write_conversation_id(key_conversation[1])
