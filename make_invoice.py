"""
Gets all the relevant information and spits out an invoice.
Gets the shouts from a saved .klat conversation file &
  the PayPal email & username(s) from the user.
"""
import toybox
import config
import os, sys
import re


def make_invoice(file=None, address=None, usernames=None, limit=None, earning=None, extra_charges=None, outfile='this-week'):
    # Load the shouts with a Cowboy and sort them
    scw = toybox.ScreenWriter(file=outfile)
    cwb = toybox.Cowboy()

    print('Loading the logs...')

    if file:
        read_file = file
    else:
        read_file = toybox.utils.get_week_name(1) + '.klat'
        read_file = read_file if input(f'Is {read_file} the correct filename? [y/n]  ').startswith('y')\
            else input('Please input the correct filename:  ')

    cwb.load_shouts(scw.read_shouts(file=read_file))

    # Get the email and username from the user
    if address is not None:
        email = address
    else:
        email = input('What\'s the PayPal email?  ')

    if usernames is not None:
        username_input = usernames
    else:
        username_input = [username for username in input('Please type all usernames separated by spaces: ').split(' ')]

    # Hire a Banker, giving him the relevant information for email & username input.
    bnk = toybox.Banker(email=email, usernames=username_input)

    # Verify the shout invoice limit & earnings per shout
    if limit is not None:
        bnk.maximum_invoice_amount = limit

    if earning is not None:
        bnk.cents_per_shout = earning

    if extra_charges is not None:
        while not bnk.verify_extra_charges_inputs(extra_charges):
            print('Those extra charges you listed looked a little funky. Can you give me the list again?')
            print('Enter /done to finish the list.')

            new_charge_list = []
            while True:
                charge_new_input = input('Charge amount:  ')

                try:
                    charge_new_input = float(charge_new_input)
                except:
                    if charge_new_input == '/done':
                        break
                    else:
                        continue

                desc_new_input = input('Charge description:  ')

                new_charge_list.append(charge_new_input)
                new_charge_list.append(desc_new_input)

                extra_charges = new_charge_list

        bnk.extra_charges = [(float(extra_charges[i]), extra_charges[i+1])
                             for i in range(0, len(extra_charges) - (len(extra_charges) % 2), 2)]

    # Get the count of all the shouts from every username the user listed.
    authors = cwb.get_author_distribution()
    bnk.shout_count = sum([authors[username] for username in username_input])

    invoice = bnk.compose_invoice()

    print(invoice)

    if input('Is this correct? [y/n]  ') == 'y':
        with open(os.path.join(config.INVOICE_FILE_PATH, outfile), 'w') as pen:
            pen.write(invoice)


def combine_invoices(weeks_back_start, weeks_back_end=0):
    """
    Read the .kinv files for the range of weeks specified in the parameters and combine
    them. Only print to the screen the combined invoice.
    :param weeks_back_start: The number of weeks prior to the current week to start
        combining invoices. Inclusive.
    :param weeks_back_end:   An optional number of weeks prior to the current week to stop
        combining invoices. Defaults to the current week.
    :return: Nothing
    """
    regex = r'.+\n.+requested: \$(\S+)\n.+address: (\S+@\S+\.\w+)\n.+completed: (\S+)\n.+used: (.+)\n.*'
    amount, count = 0, 0
    usernames = set()

    for week in range(weeks_back_start, weeks_back_end, -1):
        invoice_file = os.path.join('invoices', toybox.utils.get_week_name(weeks_back=week) + '.kinv')

        with open(invoice_file) as pen:
            file_contents = pen.read()
            r = re.match(regex, file_contents)

            if r:
                idv_amount, email, idv_count, idv_usernames = r.groups()

            else:
                print('ERROR: Invoice regex not matched in '
                      'file "{filename}"\nFile contents:\n--\n{contents}\n--\n'.format(
                          filename=invoice_file,
                          contents=file_contents if len(file_contents) > 0 else '<NO CONTENTS>'
                      ), file=sys.stderr)
                return

            email = email
            amount += float(idv_amount)
            count += int(re.sub(',', '', idv_count))

            if len(usernames) > 1:
                for name in idv_usernames:
                    usernames.add(name)
            else:
                usernames.add(idv_usernames)

    date1 = toybox.utils.get_week_name(weeks_back_start)
    date2 = toybox.utils.get_week_name(weeks_back_end+1)
    print('Combined invoice for {date1month}/{date1day} to {date2month}/{date2day}:\n'
          .format(
                date1month=date1[2:4],
                date1day=date1[4:6],
                date2month=date2[6:8],
                date2day=date2[8:10]
            ) +
          f'Total amount requested: ${amount:.2f}\n' +
          f'PayPal email address: {email}\n' +
          f'Total shouts completed: {count:,}\n' +
          'Username{plural} used: {u_list}'.format(
              plural="s" if len(usernames) > 1 else "",
              u_list=toybox.pretty_list(usernames)
          )
          )


if __name__ == '__main__':
    make_invoice()
