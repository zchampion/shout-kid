# Shout KID (**K**lat **I**nspector & **D**atalyzer)

**Overall goal:** To automagically fetch shouting statistics from the shout
wall on klat.com, provide statistics and summaries, and manage the maintenance of invoicing.

---

The KID will be organized into 4 separate modules, each named after a childlike fantasy/toy:
## Astronaut
The Astronaut goes up and fetches the information from klat.com/klatchat.php.
It "launches" into "klat space" by the method `Astronaut.launch()` and then can either
`Astronaut.fetch_conversation()` to fetch the whole conversation launched to or
`Astronaut.fetch_shouts()` to get only the shouts already loaded.

## ScreenWriter
The ScreenWriter is in charge of saving things to file and reading from files.
It must load the shouts by calling `ScreenWriter.load_shouts(shout_list)` and then
`ScreenWriter.write_shouts()` to save all shouts in the default format to a file
named for the current week.

## Cowboy
The Cowboy is the module which will herd all the shouts into groups.
It has to load the shouts by a similar method to the one used by the ScreenWriter - `Cowboy.load_shouts(shout_list)`.
Upon calling this method, the shouts will automatically be 'herded' into `Cowboy.author_bucket` & `Cowboy.hour_bucket`
attributes. The Cowboy has methods to print shouts by a specified hour or author.
At the moment, it only herds the shouts into groupings by author and time, but the plan is to
make it able to give summaries and interesting overall statistics about conversations and
shouters.

## Banker
The Banker is in charge of the invoice itself. It creates individual invoices and combines them if necessary.
It will read from the invoice files and the invoice files only in order to combine them.

---

## Installation

The KID is still under construction. As of 5/26/19, it is not ready for a full install.
However, if you're familiar with git, you may clone the repo in order to try out some of
the features locally.

Setup:
1. Clone this repo to the folder of your choosing.
2. Create a python virtual environment with at least python version 3.6.
3. Run `pip install -r requirements.txt`.

Try out any of the Shout KID's features by entering in a terminal `python kid.py {{ command }}`.  
Run `python kid.py --help` to see options.
