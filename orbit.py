#!/usr/bin/python3

"""
This is a personal script that is NOT intended for use in releases.

There are two functions defined in the file. One is called orbit and the other is
orbit_system.

The "orbit" function runs if the file is called as a script and run.
The "orbit_system" function can be called through the python environment by running
$ python3
\>>> from orbit import orbit_system
\>>> orbit_system()
"""
import os
from datetime import datetime
import logging

import selenium

import config
from toybox.Astronaut import Astronaut
from toybox.Cowboy import Cowboy
from toybox.ScreenWriter import ScreenWriter
from toybox.utils import wait, ColorCodes, pretty_list

"""
The list of times to wait in between fetching shouts. If there are no new shouts
found in a period, the time to wait will increment based on this list.
For example, if there are no shouts, the times to wait will be 1 minute, then 1 again,
then 2, then 3 minutes, and so on. When it reaches 13 minutes, if there are still no
new shouts, the time to wait will remain at that limit.
"""
fib = [1, 1, 2, 3, 5, 8]
c = ColorCodes()

logger = logging.getLogger('kid.orbit')


def orbit(chat_url=config.KLATCHAT_URL, outfile=None):
    """
    orbit runs in the background of shouting on klat.com, pulling shouts in every few
    minutes. It will (on linux) create notifications if there are new shouts, making the
    notifications persistent if it's been more than 6 minutes since the last shout.
    When finished, the user (me) must use a keyboard interrupt (ctrl + c) to stop the
    script, at which time, the user (me) will be asked if they (I) want to save the
    shouts.
    :param outfile:
    :param chat_url:
    :return: nothing returned
    """
    logger.debug(f'Starting orbit around url {chat_url}...')

    # Start up an Astronaut object from the KID toybox.
    logger.debug('Recruiting Astronaut...')
    ast = Astronaut()

    try:
        # Launch the Astronaut object to the proper (or specified) page & initialize
        # time period tracking.
        logger.info('Launching into klat space...')
        ast.launch(url=chat_url)
        period_mark = 0

        # Print the name of the conversation to the screen.
        logger.info('Orbiting around conversation: "{title}"'.format(
            title=ast.conv_title[0]))

        # Now that we know this is the right conversation, record the id.
        ScreenWriter(file=outfile).write_conversation_id(ast.conv_title[1])

        # Print the results of the fetch to the console.
        logger.info('{count:,} shouts in total.'.format(
            count=ast.conv_total
        ))

        # Now start an infinite loop for the orbit.
        while True:
            logger.debug(f'Waiting {fib[period_mark] * 60} seconds...')
            wait(fib[period_mark] * 60)

            logger.debug(f'Dismissing the disclaimer modal window...')
            ast.dismiss_modal()

            # Note the number of shouts for comparison later & fetch new shouts.
            last_shouts_len = ast.conv_total
            ast.conv_total = ast.get_current_shout_total()
            num_new_shouts = ast.conv_total - last_shouts_len if ast.conv_total else 0

            logger.debug(f'Fetching new shouts...')
            ast.fetch_shouts()
            logger.debug(f'Got {ast.conv_total} shouts, {num_new_shouts} of them new')

            # If there are new shouts, send a notification and reset the orbital period.
            if ast.conv_total > last_shouts_len:
                # Get the visible shouts to get the active authors. Uses a set.
                shouts_of_interest = ast.shouts[-1 * num_new_shouts:]
                new_authors_set = {shout.author for shout in shouts_of_interest}
                logger.debug(f"Authors gotten: {new_authors_set}\n"
                             f"From these {num_new_shouts} shouts: {shouts_of_interest}")
                new_authors = pretty_list(new_authors_set)

                # Compose the notification string
                notification = '{count} new shout{plural} found from {authors}.'.format(
                    plural='s' if num_new_shouts > 1 else '',
                    count=num_new_shouts,
                    authors=new_authors
                )

                # Send a notification for mac
                if os.uname().sysname == 'Darwin':
                    logger.debug(f'Sending Mac notification: {notification}')
                    os.system('osascript -e \'display notification "{notification_string}" with title "New Shouts!"\''
                              .format(notification_string=notification))

                # Send a notification for Linux
                elif os.uname().sysname == 'Linux':
                    logger.debug(f'Sending Linux notification: {notification}')
                    os.system('notify-send "{notification_string}" --urgency={urg}'.format(
                        notification_string=notification,
                        urg='normal' if period_mark < 4 else 'critical'
                    ))

                else:
                    logger.error(f'Unable to identify os.uname().sysname: {os.uname().sysname}')

                # Print the results of the fetch and any new shouts found.
                # TODO: Add color to the console logger
                # c.print_color('[{timestamp}]  '.format(
                #     timestamp=datetime.now().strftime(config.TIMESTAMP_FMT)
                # ), label='header', end='')
                logger.info(notification)

                # Reset the orbital period and the shout cache
                period_mark = 0

            ast.shouts = []

    except selenium.common.exceptions.StaleElementReferenceException:
        ast.close_driver()

        logger.info('Selenium Stale Element Error - Restarting...')

        orbit(chat_url=chat_url)

    # When finished, the user (me) will use ctrl+c to stop the script.
    except KeyboardInterrupt:
        # Print to the screen when the stop was initiated.
        print()
        # TODO: Add color to the console logger
        # c.print_color('[{timestamp}]'.format(
        #     timestamp=datetime.now().strftime(config.TIMESTAMP_FMT)
        # ), label='header', end=' ')
        logger.info('Stopping...'.format(
            timestamp=datetime.now().strftime(config.TIMESTAMP_FMT)))

        # Relaunch the Astronaut object to refresh the connection to the page.
        ast.launch(url=chat_url)

        # Fetch the shouts from the conversation.
        try:
            logger.info(f'Fetching conversation...')
            ast.fetch_conversation()

        except selenium.common.exceptions.StaleElementReferenceException:
            # TODO: Add color to the console logger
            # c.print_color('[{timestamp}]'.format(
            #     timestamp=datetime.now().strftime(config.TIMESTAMP_FMT)
            # ), label='header', end=' ')
            logger.info('Selenium Stale Element Error - Restarting...')
            ast.launch(url=chat_url)
            ast.fetch_conversation()

        # Now that it's not needed anymore, close down the browser
        ast.close_driver()

        # Print to the screen when the stop is successful.
        # TODO: Add color to the console logger
        # c.print_color('[{timestamp}]'.format(
        #     timestamp=datetime.now().strftime(config.TIMESTAMP_FMT)
        # ), label='header', end='')
        logger.info(' Stopped.')

        # Print a summary of the shouts from the conversation by author.
        # Hire a cowboy to herd the shouts into their pens.
        cwb = Cowboy()
        logger.debug(f'Loading the shouts with a Cowboy...')
        cwb.load_shouts(ast.shouts)

        c.print_color('Summary of shouts:', label='header')

        # Get a sorted list of authors by the number of respective shouts.
        leaderboard = cwb.get_author_distribution()

        print(leaderboard)

    except Exception as e:
        # Compose the notification string
        notification = 'Shout KID Error! ({})'.format(e)

        # Send a notification for mac
        if os.uname().sysname == 'Darwin':
            logger.debug(f'Sending the mac notification: {notification}')
            os.system('osascript -e \'display notification "{notification_string}" with title "{title}"\''
                      .format(notification_string=notification,
                              title='Error!'))

        # Send a notification for Linux
        elif os.uname().sysname == 'Linux':
            logger.debug(f'Sending the Linux notification: {notification}')
            os.system('notify-send "{notification_string}" --urgency={urg}'.format(
                notification_string=notification,
                urg='critical'
            ))

        else:
            logger.debug(f'Unable to identify os.uname().sysname: {os.uname().sysname}')


def orbit_system(orbital_period=10):
    """
    The "orbit_system" function will periodically check the list of active conversations
    and print it out so the user can keep an eye out for the desired conversation to be
    created. We assume it's purely out of laziness that the user does not simply create
    the conversation themselves, but who are we to judge? We wrote the function!
    :param orbital_period:
    :return:
    """
    # Start an Astronaut object from the KID toybox.
    logger.debug('Recruiting Astronaut...')
    ast = Astronaut()

    # Launch the Astronaut object to the proper (or specified) page & initialize
    # time period tracking.
    logger.info('Launching into klat space...')
    ast.launch()

    # Start the infinite loop
    try:
        logger.info('Orbiting around klat system...')
        while True:
            # Print the start notification to the console.
            logger.info('Fetching conversation list...')

            # Print the conversation list.
            for conversation in ast.get_conversation_list():
                c.print_color('> ', label='warning', end='')
                print(conversation)

            # Print the waiting period to the console. And do the waiting.
            logger.info('Current orbital period is {} minutes.'.format(orbital_period))
            wait(orbital_period * 60)

    # Just like in orbit, the user (me) will use a ctrl+c to stop the script.
    except KeyboardInterrupt:
        # Print to the console the time of starting to stop
        logger.info('Stopping...')

        # Print the conversation list one last time.
        ast.launch()
        for conversation in ast.get_conversation_list():
            c.print_color('> ', label='warning', end='')
            print(conversation)

        # Print to the console when everything stopped.
        logger.info('Stopped.')


if __name__ == "__main__":
    orbit()
