"""
This script goes through conversations id by id and asks the user if they
would like to save shouts from that conversation. The user stops this part
one by a keyboard interrupt (ctrl+c), at which point the script goes
through every noted conversation and gets all the shouts from it.

It will get all the shouts from every conversation, save them to the specified file,
and output a list of the total shouts per username.
"""
import logging
import re
import sys
import threading
from multiprocessing import Pool
from toybox.utils import retry

import selenium.common.exceptions as selexc

import config
import toybox

c = toybox.ColorCodes()
logger = logging.getLogger('kid.leaderboard')
critical_exceptions = []


def crawl_ids(todays_id):
    ids_list = []
    conversation_id = todays_id
    logger.debug(f'Crawling ids starting at id {todays_id}...')

    try:
        while True:
            local_astronaut = toybox.Astronaut()
            target_url = f'{config.KLATCHAT_URL}?currConv={conversation_id}'
            logger.debug(f'Launching to {target_url}...')
            local_astronaut.launch(url=target_url)
            logger.debug(f'Got conversation title: {local_astronaut.conv_title}')

            if not local_astronaut.conv_title[0].startswith('!PRIVATE') and user_chooses_conversation(local_astronaut.conv_title):
                logger.info(f'Saving conversation {local_astronaut.conv_title[0]}')
                ids_list.append(local_astronaut.conv_title[1])

            local_astronaut.close_driver()

            if conversation_id % 5 == 0:
                logger.info('Checked id {}'.format(conversation_id))

            conversation_id -= 1

    except KeyboardInterrupt:
        logger.debug(f'Gathered ids {", ".join(ids_list)}')
        return ids_list


def crawl_domain_ids(screenwriter=None):
    local_astronaut = toybox.Astronaut()
    if screenwriter:
        record_keeper = screenwriter
    else:
        record_keeper = toybox.ScreenWriter()
    local_astronaut.launch()
    conversation_list = local_astronaut.get_domain_conversation_list()
    ids_list = []

    try:
        for conversation in conversation_list:
            regx = r'(.*)-(\d{5,6}).*'
            match = re.match(regx, conversation)
            logger.debug(f'regx match: {match}')
            conversation_title = match.groups()
            logger.debug(f'regx groups: {conversation_title}')
            if user_chooses_conversation(conversation_title):
                ids_list.append(conversation_title[1])
                record_keeper.write_conversation_id(conversation_title[1])

    except KeyboardInterrupt:
        print()
        logger.info(f'Gathered domain ids {", ".join(ids_list)}')
        return ids_list

    except Exception as e:
        logger.error(f'error in crawl_domain_ids: {e}')


def user_chooses_conversation(conversation_title):
    question = 'Log shouts from {color}{title}{reset} (id: {conversation_id})? [y/n]  '.format(
        color=c.template.format(code=93),
        title=conversation_title[0],
        reset=c.template.format(code=0),
        conversation_id=conversation_title[1]
    )

    return input(question).lower() == 'y'


def launch_mission(conv_id):
    return send_astronaut(conv_id)


@retry(number_retries=3, sec_between_retry=5, logger=logger)
def send_astronaut(conv_id):
    try:
        results_list = []
        audit_sum = []

        logger.debug(f'Getting shouts from {conv_id}...')
        local_astronaut = toybox.Astronaut()
        local_astronaut.launch(url=f'{config.KLATCHAT_URL}?currConv={conv_id}')
        logger.info("Getting shouts from '" + local_astronaut.conv_title[0] + "'...")

        success = local_astronaut.fetch_conversation()
        retry_num = 0
        while not success and retry_num < 3:
            retry_num += 1
            success = local_astronaut.fetch_conversation()

        if success:
            results_list = local_astronaut.shouts

        audit_sum.append(local_astronaut.conv_total)

        if not results_list:
            msg = f"Getting shouts from '{local_astronaut.conv_title[0]}' failed!"
            logger.error(msg)
            raise Exception(f"No shouts found in {local_astronaut.conv_title[0]}")
        elif len(results_list) > local_astronaut.conv_total:
            msg = f"Too many shouts gotten from '{local_astronaut.conv_title[0]}'!"
            logger.error(msg)
            raise Exception(f"Too many shouts found in {local_astronaut.conv_title[0]}")

        return results_list, audit_sum

    except (selexc.ElementNotInteractableException, selexc.StaleElementReferenceException) as e:
        logger.warning(f"Error getting shouts from {local_astronaut.conv_title[0]} due to {type(e).__name__}")
        logger.debug({str(e).strip()})
        raise e

    except Exception as e:
        logger.critical(f"Failed to send astronaut due to {type(e).__name__}: {e}")
        raise e


def get_week_shouts(id_list, max_threads=None):
    try:
        with Pool(max_threads) as pool:
            results = pool.map(launch_mission, id_list)
            shout_results = [shout for conversation_list in results for shout in conversation_list[0]]
            audit_shouts = [conversation_list[1][0] for conversation_list in results]

        if [] in [conversation_list[0] for conversation_list in results]:
            raise Exception(f"Empty conversation found in results!")

        shout_results.sort(key=lambda shout: shout.time)

        logger.info(f"Total shouts found: {len(shout_results):,}, Total possible shouts: {sum(audit_shouts):,} "
                    f"({len(shout_results)*100/sum(audit_shouts):.2f}%)")

        return shout_results

    except Exception as e:
        logger.error(f"get_week_shouts error: {e}")
        sys.exit(1)


def get_week(manual_ids=None, manual_id_file=None, crawl_start=None, max_threads=None, screenwriter=None):
    id_list = manual_ids

    logger.debug(f'Getting week...')

    if manual_ids is None and manual_id_file is None:
        logger.debug(f'...without manual ids or manual id file.')
        if crawl_start is not None:
            id_list = crawl_ids(crawl_start)
        else:
            id_list = crawl_domain_ids(screenwriter=screenwriter)

    elif manual_id_file is not None:
        logger.debug(f'...with a manual id file.')
        if screenwriter:
            scw = screenwriter
        else:
            scw = toybox.ScreenWriter()

        id_list = scw.read_id_file(manual_id_file)

    else:
        logger.debug(f'Manual ids given: {manual_ids}.')

    if id_list is None:
        logger.critical(f'ID list is empty!')
    else:
        logger.debug(f'Sorting the id list: {id_list}')
    id_list.sort()

    shouts = get_week_shouts(id_list, max_threads)

    if not critical_exceptions:
        screenwriter.load_shouts(shouts)
        screenwriter.write_shouts(overwrite=True)
    else:
        logger.info(f"Nothing written.")


def get_leaderboard(ignore_refresh=False, manual_ids=None, manual_id_file=None, crawl_start=None, max_threads=None,
                    output_file='this-week'):
    logger.debug(f'Getting leaderboard...')

    scw = toybox.ScreenWriter(file=output_file)
    cwb = toybox.Cowboy()

    if not ignore_refresh:
        logger.debug(f'Getting shouts...')
        get_week(manual_ids=manual_ids, manual_id_file=manual_id_file, crawl_start=crawl_start, max_threads=max_threads,
                 screenwriter=scw)

    logger.debug(f'Loading shouts again...')
    cwb.load_shouts(scw.read_shouts())

    authors = cwb.get_author_distribution()

    print(authors)


if __name__ == "__main__":
    get_leaderboard()
