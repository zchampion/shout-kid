"""
This file contains miscellaneous utilities used throughout the KID program.

Shout KID
    Copyright (C) 2019  Zac Champion  zchampion270@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from time import sleep, time
from datetime import date, timedelta, datetime
import logging
import os

import config


logger = logging.getLogger('toybox.utils')


def wait(time_to_wait):
    """
    Implements a countdown that prints to the console in the same line the whole time.
    Prints in blue for most of the countdown, then when under 1/3 of the time is left,
    print in yellow, and when under 1/8 of the time is left, print in red.
    :param time_to_wait: Number of seconds to count down.
    :return: Nothing.
    """
    c = ColorCodes()
    count_fmt = '[T-{:0' + str(len(str(time_to_wait))) + '}]'

    for i in range(time_to_wait, 0, -1):
        if i <= time_to_wait / 8:
            color = 'red'
        elif i <= time_to_wait / 3:
            color = 'yellow'
        else:
            color = 'lavender'

        print('\u001b[2K', end='')
        c.print_color(string=count_fmt.format(i),
                      label=color,
                      end='\r')
        sleep(1)

    print('\u001b[2K[--{dash}]'.format(dash='-' * len(str(time_to_wait))), end='\r')


def stopwatch(func):
    """
    Decorator that times the implementation of the function it decorates.
    Prints the timing to the console.
    :param func:
    :return:
    """
    def stopwatch_wrapper(*args, **kwargs):
        start = time()
        result = func(*args, **kwargs)
        end = time()
        logger.info('Time took to run "{function_name}": {time_sec:.3f} seconds'
                    .format(function_name=func.__name__, time_sec=end-start))
        return result
    return stopwatch_wrapper


class ColorCodes:
    """
    Contains escape characters to print colors to the console.
    """
    def __init__(self):
        self.template = '\033[{code}m'
        self.labels = {
            'header': 95,
            'okblue': 94,
            'okgreen': 92,
            'warning': 93,
            'fail': 91,
            'endc': 0,
            'reset': 0,
            'bold':  1,
            'underline':  4,
            'lavender': 95,
            'yellow': 94,
            'red': 91,
        }

    def print_color(self, string, code=None, label=None, *args, **kwargs):
        """
        Prints to the screen a string in a color, then resets the color.
        :param code: The color to print to the screen in directly as the id of the color
        :param label: The color to print to the screen in, as defined in the dictionary
            attribute of the ColorCodes object.
        :param string: The string to print in color.
        :param args: Any arguments to pass on to the print function.
        :param kwargs: Any keyword arguments to pass on to the print function.
        :return:
        """
        # If the method is called by using the code parameter directly, print the corresponding escape char
        # and reset after printing the string.
        if code is not None and label is None:
            print(f'{self.template.format(code=code)}{string}{self.template.format(code=0)}', *args, **kwargs)

        # If the method is called by using the code parameter directly, print the corresponding escape char
        # by looking up the code referred to by the label using the labels dictionary and reset after
        # printing the string.
        elif code is None and label is not None:
            print(f'{self.template.format(code=self.labels[label])}{string}{self.template.format(code=0)}', *args, **kwargs)

        else:
            raise SyntaxError


def get_week_name(weeks_back=0):
    """
    Makes a string to represent a week as a name in the format
    :param weeks_back:
    :return:
    """
    # Get today as a datetime object.
    today = date.today()

    # Sunday between midnight & 1am should count as the previous week, my time.
    # TODO: Implement time zone support with configuration
    if today.weekday() == 6 and datetime.now().hour == 0:
        weeks_back += 1

    # Find the difference between today's weekday and Sunday.
    idx = (today.weekday() + 1) % 7

    # Get datetime objects for the corresponding Sunday & Saturday to the number
    # of weeks looking back.
    sun = today - timedelta((7 * weeks_back) + idx)
    sat = today - timedelta((7 * weeks_back) + idx - 6)

    # Compile the weekday.
    week_name = '{sat:%y}{sun:%m%d}{sat:%m%d}'.format(sun=sun, sat=sat)

    return week_name


def pretty_list(iterable):
    """
    Prints out a list in a format like:
    this, that, & other
    :param iterable: list or other iterable to print
    :return: a string that looks pretty
    """
    iterable = [str(item) for item in iterable]

    if len(iterable) > 2:
        string = ''

        for i in range(len(iterable)):
            string += iterable[i]

            if i < len(iterable) - 2:
                string += ', '

            elif i == len(iterable) - 2:
                string += ', & '

    elif len(iterable) == 2:
        string = iterable[0] + ' & ' + iterable[1]

    else:
        string = iterable[0]

    return string


def set_logger(name):
    """
    Create and set up the logger for the project
    :param name: The name of the logger
    :return: the logger object
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    # create file handler which logs even debug messages
    fh = logging.FileHandler(os.path.join(config.KID_LOGGING_FILE_PATH, config.KID_LOGGING_FILE_NAME))
    fh.setLevel(logging.DEBUG)

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    if 'LOGLEVEL' in os.environ:
        ch.setLevel(logging._nameToLevel[os.getenv('LOGLEVEL').upper()])

    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s | %(name)20s | %(levelname)8s: %(message)s')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)

    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)

    return logger


def retry(number_retries=4, sec_between_retry=60, blacklist=[], whitelist=[], logger=None):
    """
    Decorator that retries the function it's decorating if the function it's decorating returns an error or gets an
    error in an HTTP response.
    Works by wrapping around the function that `@retry_errors()` is put before and examining the results for errors
    or markers defined in the blacklist parameter.
    Function within function within function structure necessary so that the decorator may take parameters and leave
    alone the parameters of any function it wraps around.
    :param number_retries: The max number of times to retry the function. This number does not include the first run
    of the function. E.g. to run the function a total of 5 times for all failures, this would be 4.
    :param sec_between_retry: time in seconds to wait between retries of the function. This can be a list,
    in which case, the time between retries will increment along the list until they reach the final increment
    length, which will repeat as long as the retries keep retrying.
    :param blacklist: list of errors upon receipt of which to retry the decorated function. This can be the status
    code of an HTTP response, the message in an HTTP response, or the raw return result of the function. If this is
    empty, the function will be retried upon receipt of all errors.
    :param whitelist: list of errors that will not cause a retry. This list will supercede the blacklist; i.e. any
    errors that are in both the blacklist and the whitelist will be ignored.
    :param logger: this is so that the AWS logger may be passed in and used for logging upon retry of any function.
    :return: Returns the function it's decorating
    """
    def real_retrier(func):
        def retry_flagger(result, retry_count):
            """
            Helper function solely for retry_wrapper
            :param result:
            :param retry_count:
            :return:
            """
            # Don't retry the function if the max retries have been reached
            if retry_count >= number_retries:
                return False

            else:
                # Look for the result of the function in the retry blacklist and whitelist.
                try:
                    flag = result not in whitelist and result in blacklist

                except Exception as e:
                    if logger:
                        logger.warning(f'Unable to determine if result is in retry blacklist or whitelist: {e}')
                    flag = False

            return flag

        def retry_wrapper(*func_args, **func_kwargs):
            """
            This is where the actual retrying of the function is happening. *func_args and **func_kwargs let the
            function's original parameters through to the execution of the actual function. The results of the
            function, upon exiting this retry loop, are returned as they would have been wherever the function is used.
            :param func_args:
            :param func_kwargs:
            :return:
            """
            retry_count = 0

            # If the sec_between_retry param is a list (for dynamic retry times), extract the proper wait time from
            # the list. Otherwise, just use the number.
            if type(sec_between_retry) == list:
                if retry_count < len(sec_between_retry):
                    time_to_wait = sec_between_retry[retry_count]

                else:
                    time_to_wait = sec_between_retry[-1]

            else:
                time_to_wait = sec_between_retry

            try:
                # Try the function for the first time
                result = func(*func_args, **func_kwargs)
                # Examine the results of the function via retry_flagger
                retry_flag = retry_flagger(result, retry_count)

            except Exception as e:
                # If there was an error, retry the function unless the error's message is in the whitelist
                result = str(e)
                retry_flag = str(e) not in whitelist

            while retry_flag:
                if logger:
                    logger.warning(f'Retrying function "{func.__name__}" with args ({func_args}, {func_kwargs}) after a {time_to_wait} second wait...')
                sleep(time_to_wait)

                try:
                    # Try to retry the function
                    result = func(*func_args, **func_kwargs)
                    retry_count += 1
                    # Reexamine the results of the function to see if it needs to be retried again
                    retry_flag = retry_flagger(result, retry_count)

                except Exception as e:
                    # If there was an error, retry the function unless the error's message is in the whitelist
                    retry_flag = str(e) not in whitelist

            return result
        return retry_wrapper
    return real_retrier

