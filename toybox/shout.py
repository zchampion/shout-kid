"""
Shout object that contains the information and functions needed to describe a shout from
klat.com.

Shout KID
    Copyright (C) 2019  Zac Champion  zchampion270@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime, timedelta

import re


class Shout:
    def __init__(self, time, author, message, replies=[]):
        self.time = self._init_parse_time(time)  # Time of the shout
        self.author = author                     # Author of the shout
        self.message = message.rstrip('\n')      # Message of the shout
        self.replies = []                        # Any replies to the shout as Shout objects themselves

        for reply in replies:
            self.replies.append(Shout(time=reply[0],
                                      author=reply[1],
                                      message=reply[2]))

    def __str__(self):
        """
        Overwrites built-in object method. If there is a reply, adds an additional line
        to the string. Otherwise, it's just the one line.
        :return: Formatted string in the form:
        On Friday at 12:48PM, TheFluffyQ said, "How's it going?"
            Then on Friday at 2:56PM, KatharineSwan replied, "Good."
            Then on Friday at 2:56PM, KatharineSwan replied, "How about you?"
        """
        shout_string = 'On {timestamp}, {author} said, "{message}"'.format(
            timestamp=self.time.strftime('%A at %I:%M%p'),
            author=self.author,
            message=self.message
        )

        if len(self.replies) > 0:
            for reply in self.replies:
                shout_string += '\n    Then on {timestamp}, {author} replied, "{message}"'.format(
                    timestamp=reply.time.strftime('%A at %I:%M%p'),
                    author=reply.author,
                    message=reply.message
                )

        return shout_string

    def __repr__(self):
        """
        Defines the easy-to-parse string format. This is the format that's used when
        saving shouts to file.
        :return: String in the form:
        2019-02-15T12:48:13|TheFluffyQ|How's it going?|None|None|None
        """
        r_string = "{timestamp}|{author}|{message}".format(
            timestamp=self.time.strftime('%Y-%m-%d %H:%M:%S'),
            author=self.author,
            message=self.message
        )

        if len(self.replies) > 0:
            for reply in self.replies:
                r_string += '|{}|{}|{}'.format(
                    reply.time,
                    reply.author,
                    reply.message
                )

        return r_string

    def __eq__(self, other):
        """
        Defines that a shout is equal to another if its time, author, and message are the same.
        :param other: Another shout object to compare to.
        :return: Boolean value whether the objects are equal.
        """
        assert type(self) == type(other)
        return self.time == other.time and \
            self.author == other.author and \
            self.message == other.message

    def __hash__(self):
        """
        Defines the hash of the shout object.
        :return:
        """
        return hash(repr(self))

    def _init_parse_time(self, time_str):
        """
        Parses the timestamp that comes from the shout wall. The way those timestamps
        are formed is strange during the hour of 12am, so the exception takes care of
        that.
        :param time_str: The timestamp from the html shout bubble object.
        :return: The corresponding datetime object.
        """
        if re.match(r'^[A-Z][a-z]{2} \d{1,2}:\d{2}[ap]m$', time_str):
            return self.parse_time_string(time_str)

        else:
            try:
                # In most cases, this format string should work.
                return datetime.strptime(time_str, '%Y-%m-%d %I:%M:%S%p')

            except ValueError:
                try:
                    # If it doesn't, it's because it's the hour of 12am and the timestamp is odd.
                    # This other format will take care of it.
                    return datetime.strptime(time_str, '%Y-%m-%d %H:%M:%S%p')

                except ValueError:
                    return datetime.strptime(time_str, '%Y-%m-%d %H:%M:%S')

    @staticmethod
    def parse_time_string(time_string):
        """
        Parses the limited timestamp that's printed to the screen based on the current
        date.
        :param time_string: Timestamp in the format 'Wed 2:48PM'
        :return: Datetime object
        """
        # Define constants for parsing the day and the time of the string.
        klat_weekdays = ['Mon', 'Tue', 'Wed', 'Thr', 'Fri', 'Sat', 'Sun']
        parse_format = '%I:%M%p'

        # Split the string into its day and time portions.
        weekday_split = time_string.split()

        # Figure out how many days ago or ahead the occurrence of the weekday in the
        # current week is/was.
        days = klat_weekdays.index(weekday_split[0].strip()) - datetime.today().weekday()
        day_delta = timedelta(days=days)

        # Try to parse the time section based on the default.
        try:
            shout_time = datetime.strptime(weekday_split[1], parse_format)

        # If it fails, it's because it's the hour of 12am, so try the other format.
        except ValueError:
            shout_time = datetime.strptime(weekday_split[1], '%H:%M%p')

        # If the weekday has happened in the current week, simply add the negative
        # timedelta to today to find the last occurred date.
        if day_delta < timedelta(days=0):
            shout_day = datetime.today().date() + day_delta

            # Triumphantly return the last occurred date.
            return datetime.combine(shout_day, shout_time.time())

        # If the weekday hasn't happened yet in the current week, subtract 7 days from
        # the timedelta, then add the timedelta to today to get the last occurred date.
        elif day_delta > timedelta(days=0):
            day_delta = day_delta - timedelta(days=7)
            shout_day = datetime.today().date() + day_delta

            # Triumphantly return the last occurred date.
            return datetime.combine(shout_day, shout_time.time())

        # If the day is today, then just grab today's datetime and combine it with
        # the shout time.
        else:
            # Triumphantly return the date.
            return datetime.combine(datetime.today().date(), shout_time.time())

    @staticmethod
    def combine_date_and_time(date, time):
        return datetime.combine(date.date(), time.time()).strftime('%Y-%m-%d %H:%M:%S%p')

    def old_format(self):
        """
        Format the shout to mimic the legacy format from the old iteration of the
        Shout Log project.
        :return:
        """
        old_format = f'[Shout] {self.time.strftime("%a %m.%d %H:%M:%S")} "{self.message}"'

        for reply in self.replies:
            old_format += f'\n[Shout] {reply.time.strftime("%a %m.%d %H:%M:%S")} "{reply.message}"'

        return old_format

    def to_dict(self):
        """
        Return the shout as a dictionary for easy processing with dataframes.
        :return: dictionary with shout attributes as rows
        """
        shout_dict = {
            'date': self.time.date(),
            'time': self.time.time(),
            'author': self.author,
            'message': self.message,
            'replies': self.replies
        }

        return shout_dict
