"""
    Shout KID
    Copyright (C) 2019  Zac Champion  zchampion270@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import logging
import re

import numpy as np
import pandas as pd
import config

logger = logging.getLogger('toybox.Cowboy')
pd.set_option('display.max_rows', None)


class Cowboy:
    """
    Wrangles the shouts into a DataFrame for analysis.
    Will need to answer questions like:
    x What does the distribution of shouts look like by author for a pool of shouts?
    x What does the distribution look like by hour of the day and day of the week?
    x When is an author most likely to be on the shout wall by hour of the day and day of the week?
    x What are the most common words for each author?
    x How much has an author used a particular word?
    - What do an author's shouts look like (min, max, avg, med, std) for things like:
        - characters per shout
        - words per shout
        - time between shouts
    """
    def __init__(self):
        logger.debug(f'Initializing the cowboy...')

        self.raw_shouts = []
        self.flat_shouts = []
        self.shout_df = pd.DataFrame()
        self.description_df = None

    def load_shouts(self, unsorted):
        logger.info(f'Loading shouts...')
        logger.debug(f'Length of all_shouts list: {len(self.raw_shouts)}')
        logger.debug(f'Shape of shout_df: {self.shout_df.shape}')

        self.raw_shouts = unsorted
        self.flat_shouts = self.flatten_shouts()

        self.shout_df = pd.DataFrame([shout for shout in self.flat_shouts])
        self.shout_df['date'] = pd.to_datetime(self.shout_df['date'])
        self.shout_df['hour'] = pd.to_datetime(self.shout_df['time'], format='%H:%M:%S').dt.hour
        self.shout_df['time'] = pd.to_datetime(self.shout_df['time'], format='%H:%M:%S').dt.time
        self.shout_df['weekday'] = pd.Categorical(self.shout_df['date'].dt.weekday_name,
                                                  ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'])

        logger.debug(f'Loaded shouts.')
        logger.debug(f'Length of all_shouts list: {len(self.raw_shouts)}')
        logger.debug(f'Shape of shout_df: {self.shout_df.shape}')

    def flatten_shouts(self):
        logger.debug(f'Flattening shouts...')
        shout_buffer = []

        for shout in self.raw_shouts:
            shout_buffer.append(shout.to_dict())

            if len(shout.replies) > 0:
                shout_buffer += [reply.to_dict() for reply in shout.replies]

        return shout_buffer

    def get_author_distribution(self):
        """
        Get the DataFrame with a summary of number of shouts by author.
        :return:
        """
        logger.debug(f'Sorting shouts by author...')

        distribution_df = self.shout_df.groupby('author').count().sort_values(self.shout_df.columns[0], ascending=False)
        distribution_df = distribution_df[[self.shout_df.columns[0]]][self.shout_df.columns[0]]
        distribution_df.name = 'Shout Count'

        return distribution_df

    def get_hour_distribution(self, author='everyone'):
        logger.debug(f'Sorting shouts by hour for {author}...')

        if author == 'everyone':
            pivot_tab = pd.pivot_table(self.shout_df, index=['weekday'], columns=['hour'], aggfunc=len)
        else:
            pivot_tab = pd.pivot_table(self.shout_df.where(self.shout_df['author'] == author),
                                       index=['weekday'], columns=['hour'], aggfunc=len)

        pivot_tab.sort_values('weekday')
        pivot_tab = pivot_tab[self.shout_df.columns[0]].T
        hour_index = pd.Index(np.arange(24), name='hour')
        pivot_tab = pivot_tab.reindex(hour_index).fillna(0).astype(int)

        return pivot_tab

    def get_summary_of_words(self, author='everyone', min_freq=0, topn=None):
        """
        Get all of the words in shouts and the counts.
        :param topn:
        :param min_freq:
        :param author:
        :return:
        """
        logger.debug(f'Getting summary of words for {author}...')
        word_dict = {}

        if author == 'everyone':
            messages_df = self.shout_df['message']
        else:
            messages_df = self.shout_df[self.shout_df['author'] == author]['message']

        for message in messages_df:
            for word in message.split(' '):
                word = re.sub("[^\w']", '', word.upper())
                if word in word_dict:
                    word_dict[word] += 1
                else:
                    word_dict[word] = 1

        prepare = {'word': list(word_dict.keys()), 'count': [word_dict[key] for key in word_dict]}

        words_df = pd.DataFrame(prepare).sort_values('count', ascending=False).reset_index()[['word', 'count']]
        meaningful_df = words_df[~words_df['word'].isin(config.COMMON_WORDS)].reset_index()
        common_df = words_df[words_df['word'].isin(config.COMMON_WORDS)].reset_index()

        if min_freq > 0:
            meaningful_df = meaningful_df[meaningful_df['count'] > min_freq]
        elif topn is not None:
            meaningful_df = meaningful_df.head(topn)
            common_df = common_df.head(topn)

        return meaningful_df, common_df

    def get_word_rank(self, word, author=None, word_ranks=None):
        """
        Find, in the ranking of most used words (for a single author if applicable),
        the place of a particular word of interest.
        :param word:
        :param author:
        :return:
        """
        word = word.strip(' .,?!').upper()

        if word_ranks is None:
            meaningful_word_ranks, common_word_ranks = self.get_summary_of_words(author)
        elif type(word_ranks) == tuple and len(word_ranks) == 2:
            meaningful_word_ranks, common_word_ranks = word_ranks
        else:
            raise ValueError(f'word_ranks passed in as {type(word_ranks).__name__}, not a tuple!')

        if word in config.COMMON_WORDS:
            word_row = common_word_ranks[common_word_ranks['word'] == word]
        else:
            word_row = meaningful_word_ranks[meaningful_word_ranks['word'] == word]

        return word_row

    def describe_shouts_of(self, author='everyone'):
        """
        Get statistics such as:
        - characters per shout
        - words per shout
        ...for an author.
        :param author:
        :return:
        """
        logger.debug(f'Describing shouts of {author}...')

        if author == 'everyone':
            messages_df = self.shout_df['message']
        else:
            messages_df = self.shout_df[self.shout_df['author'] == author]['message']

        self.description_df = pd.DataFrame()
        self.description_df['characters'] = messages_df.str.len()
        self.description_df['words'] = messages_df.str.split().apply(len)

        return self.description_df.describe()

