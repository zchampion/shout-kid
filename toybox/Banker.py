"""
The Banker is the module in the Shout KID that is responsible for compiling the invoice.
"""
from toybox.utils import get_week_name, pretty_list
import config
import logging


logger = logging.getLogger('kid.Banker')


class Banker:
    def __init__(self, email, usernames=[], shout_count=0):
        self.usernames = set(usernames)
        self.paypal_email = email
        self.shout_count = shout_count
        self.cents_per_shout = config.CENTS_PER_SHOUT
        self.maximum_invoice_amount = config.MAXIMUM_INVOICE_AMOUNT
        # List of tuples. For each tuple which represents a charge, item 0 is the amount and item 1 is the description.
        self.extra_charges = []
        logger.debug(f'Banker generated with usernames {self.usernames}, paypal email {self.paypal_email}, '
                     f'shout count {self.shout_count}, cents per shout {self.cents_per_shout}, '
                     f'maximum invoice amount {self.maximum_invoice_amount}, and extra charges {self.extra_charges}')


    @staticmethod
    def verify_extra_charges_inputs(charges):
        for item_idx in range(0, len(charges), 2):
            try:
                float(charges[item_idx])
            except:
                return False

        return True

    def calculate_shout_earnings(self):
        logger.info(f'Calculating earnings from shouts only...')

        shouts_charge = self.shout_count * self.cents_per_shout * 0.01

        return shouts_charge if shouts_charge <= self.maximum_invoice_amount else self.maximum_invoice_amount

    def calculate_total_earnings(self):
        logger.debug(f'Calculating total earnings...')
        shout_earnings = self.calculate_shout_earnings()
        extra_charges = sum([float(charge[0]) for charge in self.extra_charges])

        return shout_earnings + extra_charges

    def compose_invoice(self):
        logger.debug(f'Composing the invoice...')
        week_name = get_week_name(weeks_back=1)

        invoice = 'Shout Wall Invoice - Week of {month1}/{sunday} to {month2}/{saturday}\n'.format(
                        month1=week_name[2:4],
                        sunday=week_name[4:6],
                        month2=week_name[6:8],
                        saturday=week_name[8:10]) + \
                  f'Total amount requested: ${self.calculate_total_earnings():.2f}\n' + \
                  f'PayPal email address: {self.paypal_email}\n' + \
                  f'Total shouts completed: {self.shout_count:,}\n' + \
                  f'Username{"s" if len(self.usernames) > 1 else ""} used: {pretty_list(self.usernames)}\n'

        if len(self.extra_charges) > 0:
            invoice += 'Extra amounts included for:\n*  {extra_charges}\n'.format(
                extra_charges='\n*  '.join([f'{charge[1]} - ${charge[0]:.2f}' for charge in self.extra_charges])
            )

        return invoice
