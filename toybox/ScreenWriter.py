"""
The Screenwriter saves shouts to a file and loads them.

In the workflow, the Astronaut can get the shouts and give them to the ScreenWriter to
save as a whole conversation or the Cowboy can sort them and give an abridged list to
the ScreenWriter to save.

Shout KID
    Copyright (C) 2019  Zac Champion  zchampion270@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime, date
import logging
import os
import re

from toybox.shout import Shout
from toybox.utils import get_week_name


logger = logging.getLogger('toybox.Screenwriter')


class ScreenWriter:
    def __init__(self, file=None):
        self.shout_buffer = None
        self.base_dir = os.path.abspath(os.path.curdir)  # The directory the KID is being run from.
        self.data_dir = 'logs'  # The folder in which the shout files will be saved.
        self.audit_dir = 'audit'  # Folder in which backup, archive, and other old data to save will be stored.
        self.ids_dir = 'ids'

        if file == 'this-week':
            self.file = self.generate_week_filename(weeks_back=0)
        elif file == 'last-week':
            self.file = self.generate_week_filename(weeks_back=1)
        else:
            self.file = file

        logger.debug(f'Initialized Screenwriter with file {self.file}, base directory {self.base_dir}, log directory {self.data_dir}, '
                     f'and id file directory {self.ids_dir}')

    def load_shouts(self, shout_pool):
        """
        Loads the shouts from the parameter into the object's storage.
        :param shout_pool: A list of shouts.
        :return: Nothing
        """
        logger.debug(f'Loading shouts...')
        self.shout_buffer = shout_pool

    def clear_shouts(self):
        """
        Clears the ScreenWriter's shout buffer.
        :return: Nothing
        """
        logger.debug(f'Clearing shouts...')
        self.shout_buffer = None

    def _archive_old_file(self, filename):
        """
        Saves old data in a file in the audit folder.
        :param filename: name of the file to archive.
        :return: nothing
        """
        logger.debug(f'Archiving {filename}...')

        audit_filename = '_'.join(filename.split(os.path.sep))
        new_filename = '' + audit_filename
        output_file_idx = 0
        while os.path.exists(os.path.join(self.base_dir, self.audit_dir, new_filename)):
            logger.debug(f'{new_filename} exists in audit.')
            name, ext = audit_filename.split('.')
            output_file_idx += 1
            new_filename = os.path.join(f'{name}_{output_file_idx}.{ext}')

        destination = os.path.join(self.base_dir, self.audit_dir, new_filename)
        logger.debug(f'Moving {filename} to {destination}...')

        os.rename(os.path.join(self.base_dir, filename), destination)

        logger.debug(f'File {filename} archived.')

    def write_shouts(self, file=None, overwrite=False):
        """
        Asks the user what file to save as and saves the shouts from the ScreenWriter's
        buffer in the default format.
        :param file:
        :param overwrite - a flag to overwrite the file to be written if already present.
        :return: Nothing
        """

        # Compile the complete string to write to the file.
        manuscript = '\n'.join([repr(shout) for shout in self.shout_buffer]) + '\n'

        # Make sure the filename is correct.
        if file:
            filename = file
        elif self.file:
            filename = self.file
        else:
            filename = self.verify_filename()
        complete_file = os.path.join(self.base_dir, self.data_dir, filename)

        # Apply the overwrite flag if the file should be entirely overwritten.
        write_mode = 'w' if overwrite else 'a'

        if write_mode == 'w' and os.path.exists(complete_file):
            self._archive_old_file(os.path.join(self.data_dir, filename))

        logger.debug(f'Writing shouts to file {complete_file}{" with overwrite" if overwrite else ""}...')

        # Write the manuscript string to the file to save all the shouts.
        with open(complete_file, write_mode) as pen:
            pen.write(manuscript)

        # Print a receipt of how many shouts were written to the console.
        logger.info('{len_shouts:,} shouts written to \'{filename}\'{overwrite}.'.format(
            len_shouts=len(self.shout_buffer),
            filename=complete_file,
            overwrite=" with overwrite" if overwrite else ""))

    def write_shouts_old_format(self, overwrite=False):
        """
        Write shouts, but in the old shoutlog format. This method will not be included in
        releases.
        :return:
        """
        # Compile the complete string, filtered for my shouts, to write to the file.
        shout_list = [shout.old_format() for shout in self.shout_buffer
                      if shout.author == 'TheFluffyQ']
        manuscript = '\n'.join(shout_list) + '\n'

        # Compose the file name to save to.
        filename = 'shouts_{:%m-%d}.txt'.format(date.today())

        # Apply the overwrite flag if the file should be entirely overwritten.
        write_mode = 'w' if overwrite else 'a'

        if write_mode == 'w' and os.path.exists(filename):
            self._archive_old_file(os.path.join(self.data_dir, filename))

        # Write the shouts to the file.
        complete_file = os.path.join(self.base_dir, filename)
        with open(complete_file, write_mode) as pen:
            pen.write(manuscript)

        # Print a receipt of shouts written to the console.
        logger.info('{len_shouts} shouts written in the old way to \'{filename}\'{overwrite}.'.format(
            len_shouts=len(self.shout_buffer),
            filename=complete_file,
            overwrite=" with overwrite" if overwrite else ""))

    def verify_filename(self):
        """
        Generates a default filename for saving shouts to.
        Asks the user if they'd like to correct the filename and verifies any corrections.
        :return: The filename as a string.
        """
        logger.debug(f'Verifying the filename...')

        # Get the default filename.
        filename = self.generate_week_filename()

        # Ask the user for approval of the filename, for correction if approval is denied,
        # and loop until approval is given.
        while True:
            the_input = input('Is \'{}\' the correct filename?  [y/n] '.format(filename))
            if the_input.lower() == 'y':
                break

            logger.debug(f'Received input from user "{the_input}". Asking for filename correction...')

            filename = input('Please input the correct filename:  ')

            logger.debug(f'Correction received: "{filename}"')

            if filename == 'this-week':
                filename = self.generate_week_filename(weeks_back=0)
            elif filename == 'last-week':
                filename = self.generate_week_filename(weeks_back=1)

        return filename

    def read_shouts(self, file=None):
        """
        Reads shouts from a file written in the default format.
        :return: A list of Shout objects read from the file.
        """
        # Verify the filename to read from.
        if file:
            if file == 'this-week':
                filename = self.generate_week_filename(weeks_back=0)
            elif file == 'last-week':
                filename = self.generate_week_filename(weeks_back=1)
            else:
                filename = file
        elif self.file:
            filename = self.file
        else:
            filename = self.verify_filename()
        logger.debug(f'Reading shouts from {filename}...')

        shout_buffer = []

        try:
            if os.path.isfile(os.path.join(self.base_dir, self.data_dir, filename)):
                # Access the file to read.
                with open(os.path.join(self.base_dir, self.data_dir, filename)) as record:
                    for entry in record:
                        # Split the line into components.
                        entry_components = entry.split('|')

                        # Get replies to the shout if they are saved in the line
                        entry_replies = [(
                            entry_components[i],
                            entry_components[i+1],
                            entry_components[i+2],
                        ) for i in range(3, len(entry_components) // 3, 3)] if len(entry_components) > 3 else []

                        # Make a Shout object from the split components and append it to the list
                        # of shouts to return.
                        shout_buffer.append(
                            Shout(time=entry_components[0],
                                  author=entry_components[1],
                                  message=entry_components[2],
                                  replies=entry_replies)
                        )

        except Exception as e:
            logger.error(f'Error in read_shouts: {e}')

        return shout_buffer

    def read_shouts_old_format(self):
        """
        Reads shouts from a file written in the old shoutlog format.
        :return: A list of Shout objects read from the file.
        """
        # Verify the filename to read from.
        filename = self.verify_filename()
        logger.debug(f'Reading shouts in the old way from {filename}...')

        shout_buffer = []

        try:
            # Access the file to read.
            with open(os.path.join(self.base_dir, self.data_dir, filename)) as record:
                for entry in record:
                    # Extract the shout date and message from the line.
                    info = re.match(r'\[Shout\] (\w{3} \d\d.\d\d \d\d:\d\d:\d\d) "(.*)"',
                                    entry)

                    # If the line is actually a shout and not a bug record, test record, or
                    # header line...
                    if info:
                        # Reformat the extracted time to the default shout format's timestamp.
                        time = datetime.strftime(
                            datetime.strptime(info.group(1), '%a %m.%d %H:%M:%S'), '%Y-%m-%d %I:%M:%S%p')

                        # Grab the message component.
                        message = info.group(2)

                        # Assume the author is TheFluffyQ since this is the old format.
                        author = 'TheFluffyQ'

                        # Put the resulting Shout object on the list to be returned.
                        shout_buffer.append(Shout(time=time, author=author, message=message))

        except Exception as e:
            logger.error(f'Error in read_shouts_old_format: {e}')

        return shout_buffer

    def write_conversation_id(self, id_num):
        """
        Records the conversation id in a special folder and file type.
        If there is already a file with the correct week name in it, the id
        will be appended to the file on the same line.
        Each file will be formatted thusly:
        12345 12456 12678 12892
        :return: Nothing
        """
        if self.file == 'this-week':
            filename = os.path.join(self.ids_dir, get_week_name(weeks_back=0) + '.kcid')
        elif self.file == 'last-week':
            filename = os.path.join(self.ids_dir, get_week_name(weeks_back=1) + '.kcid')
        elif self.file:
            filename = self.file.replace('.klat', '.kcid')
            if not filename.endswith('kcid'):
                logger.warning(f"File {self.file} is being used as an id storing file but does not end in .kcid")
            filename = os.path.join(self.ids_dir, filename)
        else:
            filename = os.path.join(self.ids_dir, get_week_name() + '.kcid')

        if os.path.isfile(filename):
            with open(filename) as reader:
                recorded = id_num in [num.strip() for num in reader.read().split()]
        else:
            recorded = False

        if not recorded:
            with open(filename, 'a') as pen:
                pen.write(f'{id_num} ')
            logger.info(f'ID {id_num} recorded.')
        else:
            logger.info(f'ID {id_num} was already recorded.')

    def read_id_file(self, manual_id_file):
        """
        Reads an id list file and returns the list of id numbers
        :param manual_id_file:
        :return:
        """
        if manual_id_file == 'this-week':
            manual_id_file = get_week_name(weeks_back=0) + '.kcid'

        elif manual_id_file == 'last-week':
            manual_id_file = get_week_name(weeks_back=1) + '.kcid'

        logger.debug(f'Reading id file {manual_id_file}...')

        with open(os.path.join(self.ids_dir, manual_id_file)) as id_contents:
            ids = id_contents.read()

        id_list = [int(id_num) for id_num in ids.split()]

        logger.debug(f'ID numbers returned: {id_list}')

        return id_list

    @staticmethod
    def generate_week_filename(weeks_back=0):
        """
        Generates a default filename to save current shouts to.
        :param weeks_back: Integer of how many weeks back to generate a filename for.
        :return: A string with the filename generated.
        """
        return get_week_name(weeks_back) + '.klat'
