"""
The Astronaut goes up and fetches the shouts and HTML from klat.com/klatchat.php

Shout KID
    Copyright (C) 2019  Zac Champion  zchampion270@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# System imports
import os
import re
import sys
import logging
from time import sleep, time
import random

# Package imports
from selenium import webdriver
from selenium.common.exceptions import *
from selenium.webdriver.firefox.options import Options

# Internal imports
import config
from toybox.shout import Shout
from toybox.utils import wait, stopwatch

logger = logging.getLogger('toybox.Astronaut')


class Astronaut:
    def __init__(self):
        self.shouts = []  # The master list of shouts fetched from (a) conversation(s)
        self.last_updated = 0  # The length of the shout list at last update
        self.last_last_updated = 0  # The length of the shout list the time before that
        self.options = None  # The options object for the headless browser
        self.starship = None  # The instance of the headless firefox browser
        self.raw_html = ''  # The raw html of the conversation page
        self.conv_title = None
        self.conv_total = 0

        os.environ["PATH"] += os.pathsep + config.DRIVER_DIR_PATH

    def __del__(self):
        logger.debug(f'Astronaut coming back to the planet...')
        if self.starship:
            self.starship.quit()
            logger.debug('Astronaut landed.')

    def launch(self, url=config.KLATCHAT_URL):
        """
        Sends the headless browser to the url provided or klat.com/klatchat.php by
        default and waits to make sure the page is loaded and rendered.
        :param url: The optional url to load
        :return:  Nothing
        """
        # Add the headless option to the browser so it doesn't spawn a graphical window.
        logger.info(f'Launching astronaut to {url}...')
        self.options = Options()
        if 'HEADLESS' not in os.environ or os.getenv('HEADLESS').upper() != "FALSE":
            self.options.add_argument('--headless')
        self.options.add_argument('--new-window')

        # Create the headless browser.
        self.starship = webdriver.Firefox(executable_path=config.DRIVER_PATH,
                                          options=self.options)

        # Load the page with the headless browser.
        self.starship.get(url)

        # Save the raw rendered html of the page.
        self.raw_html = self.starship.page_source

        # Wait to make sure the page is loaded.
        start = time()
        i = 0
        while True:
            try:
                self.dismiss_modal()
                break
            except (NoSuchElementException, ElementNotInteractableException):
                t = (2 ** i) / 2
                if time() - start > 30:
                    raise TimeoutError(f"Too long waiting for the modal!")
                logger.debug(f"Waiting {i} time{'s' if i > 1 else ''} for {t:.1f} sec...")
                sleep(t)
                i += 1
            except Exception as e:
                logger.error(f"{type(e).__name__}: {e}")
        logger.debug(f'Done waiting.')

        # Get the title and shout number.
        self.conv_title = self.get_current_conversation_title()
        self.conv_total = self.get_current_shout_total()

    def dismiss_modal(self):
        # Click accept on the pop-up about conforming to all applicable laws.
        logger.debug(f'Dismissing the popup...')
        accept_button = self.starship.find_element_by_xpath("//button[./span[text()='Continue']]")
        accept_button.click()
        sleep(1)

    # @stopwatch
    def fetch_shouts(self):
        """
        Gets the shouts immediately visible on the conversation page.
        Saves all shouts to the shouts attribute.
        :return: Nothing
        """
        logger.debug(f'Fetching shouts...')
        # Create a list for the shouts as a buffer
        new_shouts = []
        span_elements = []

        # Find all of the elements that have a class name that starts with 'shout bubble'.
        while len(span_elements) == 0:
            span_elements = self.starship.find_elements_by_xpath(
                '//span[starts-with(@class, "shout-bubble")]'
            )
        logger.debug(f'{len(span_elements)} span elements found.')

        # For each shout, make an object and put it in the shout buffer list.
        for element in span_elements:
            self.process_shout(element, new_shouts)

        # Add the shout buffer to the shout attribute list.
        # TODO: Import the describe_iterable function
        logger.debug(f'{len(new_shouts)} new shouts found')
        self.shouts += new_shouts

        # Update the lengths of the last updated markers so it's known which shouts are
        # new.
        logger.debug(f'Before update. last_last_updated: {self.last_last_updated}, last_updated: {self.last_updated}')
        if self.last_last_updated != self.last_updated:
            self.last_last_updated = self.last_updated
        self.last_updated = len(self.shouts)
        logger.debug(f'After update. last_last_updated: {self.last_last_updated}, last_updated: {self.last_updated}')

        # Sort the list of shouts in the object in chronological order.
        self.shouts.sort(key=lambda sht: sht.time)

    def fetch_conversation(self, retry=0):
        """
        Gets all of the shouts in the whole conversation and replaces the contents of the
        shouts attribute with the results.
        :return:
        """
        logger.debug(f'Fetching conversation {retry if retry > 0 else ""}...')

        # Find the Get 100 More Shouts button if there is one
        try:
            logger.debug(f'Expanding by "Get more shouts"...')
            for count in range(100):
                logger.debug(f'Expanding {count}...')
                try:
                    self.starship.find_element_by_class_name('ec-more-log').click()
                except ElementClickInterceptedException as e:
                    try:
                        self.dismiss_modal()
                    except:
                        sleep(0.1 * count)
                        pass
                    logger.warning(f"Element intercepted while getting more shouts in {self.conv_title[0]} "
                                   f"due to {type(e).__name__}.")
                    logger.debug(str(e).strip())
            msg = f"Unable to get the shouts for {self.conv_title[0]}. " \
                  f"'Get more shouts' isn't getting any more shouts when clicked."
            logger.warning(msg)
            raise TimeoutError(msg)

        except NoSuchElementException:
            logger.debug(f'Done. Getting the loaded shouts...')
            self.fetch_shouts()

            updated = self.last_updated - self.last_last_updated
            if len(self.shouts) < 0.9 * self.conv_total:
                logger.debug(msg=f"Got {updated:,} / {self.conv_total:,} shouts ({updated*100/self.conv_total:.2f}%).")
                if retry < 16:
                    self.shouts = []
                    self.fetch_conversation(retry=retry+1)
            elif len(self.shouts) % 50 == 0:
                logger.warning(f"Number of shouts found in '{self.conv_title} is enough, but sus ({len(self.shouts)}). "
                               f"Trying one more time to be sure...'")
                self.fetch_conversation(retry=retry)
            else:
                logger.info(f'{len(self.shouts):,} shouts found out of {self.conv_total:,} in "{self.conv_title[0]}".')

            # Update the last updated markers so it's known which shouts are new.
            logger.debug(f'Before update. last_last_updated: {self.last_last_updated}, last_updated: {self.last_updated}')
            if self.last_last_updated != self.last_updated:
                self.last_last_updated = self.last_updated
            self.last_updated = len(self.shouts)
            logger.debug(f'After update. last_last_updated: {self.last_last_updated}, last_updated: {self.last_updated}')

            # Save the html of the page.
            self.raw_html = self.starship.page_source

            return True

        except Exception as e:
            logger.error(f"Error fetching conversation '{self.conv_title[0]}' due to {type(e).__name__}")
            logger.debug(f"{str(e).strip()}")
            return False

    @staticmethod
    def shout_in_list(shout, shout_list):
        for existing_shout in shout_list:
            if shout == existing_shout:
                return True

        return False

    def process_shout(self, element, shout_list):
        """
        Adds a Shout object from a shout bubble element to a list.
        :param element: shout bubble element from the headless browser.
        :param shout_list: the list to add the created Shout object to.
        :return: Nothing
        """
        try:
            # Extract the shout's author, time, message into a tuple.
            shout_attr = self.extract_shout_attr(element)
            replies_attr = self.extract_replies_attr(element)

            if shout_attr is not None:
                # If the extraction was successful, build a Shout object.
                shout = Shout(time=shout_attr[0], author=shout_attr[1], message=shout_attr[2], replies=replies_attr)

                # If the constructed shout is not already in the list, add it.
                if not self.shout_in_list(shout, shout_list):
                    shout_list.append(shout)

        # If the regex didn't return an object, do nothing. Otherwise, print the error
        # to the console along with the element that failed.
        except TypeError as passable:
            if str(passable) == '\'NoneType\' object is not iterable':
                logger.debug(passable)

            else:
                logger.error('[TypeError] {0}\n||{1}||'.format(passable, element.get_attribute('innerHTML')))

        # In the case of a mysterious error, print it to the console along with the
        # element that caused it.
        except Exception as wtf:
            logger.error('[Mysterious Error] {0}\n||{1}||'.format(wtf, element.get_attribute('innerHTML')), file=sys.stderr)

    @staticmethod
    def extract_shout_attr(element):
        """
        Extract the attributes of the shout bubble element using regex from the html of
        the element.
        :param element: The shout bubble element grabbed by the headless browser.
        :return: A tuple containing:
            0: The time of the shout
            1: The author of the shout
            2: The message of the shout
        """
        # Get the raw html of the element.
        shout_html = element.get_attribute('innerHTML')
        # logger.debug(f'extract_shout_attr: shout html = {shout_html}')

        # Define the regex that will match to the shout html.
        shout_regex = r'.*title="(\d{4}-\d{1,2}-\d{1,2} \d{1,2}:\d{1,2}:\d{1,2}[ap]m)[\w\W]*?>\s(.*?)\s<.*<span id="' \
                      r'(?:msg-)?\d*?">(.*?)<.*'

        # Find a match between the shout and the regex if there is one.
        rematch = re.match(shout_regex, shout_html)

        if rematch:
            # Return the captured attributes as a tuple.
            return rematch.groups()

        else:
            logger.debug(f'extract_shout_attr :: Regex match not found in: {shout_html}')
            return None

    @staticmethod
    def extract_replies_attr(element):
        """
        Extract the attributes of the shout bubble element replies by using regex from the html of the element.
        :param element: The shout bubble element grabbed by the headless browser
        :return: A list of tuples containing the time, author, and message of any and all replies in a shout bubble.
        """
        # Get the raw html of the element
        shout_html = element.get_attribute('innerHTML')
        # logger.debug(f'extract_replies_attr: shout html = {shout_html}')

        # Define the regex that will match to the shout html.
        reply_regex = r"(?:&nbsp;){4}<small>(.+?): .+?;\"> (\w+) .+?>  (.*?)</small>"

        # Find any and all matches between the html and the regex if there are any.
        try:
            rematches = re.finditer(reply_regex, shout_html)
            reply_list = [rematch.groups() for rematch in rematches]

            if reply_list:
                logger.debug(f'extract_replies_attr :: Replies found: {reply_list} from shout {shout_html}')

            return reply_list

        except Exception as e:
            logger.error(f'extract_replies_attr :: error: {e}')
            return None

    def print_shouts(self):
        """
        Prints all of the shouts in the Astronaut's shouts attribute
        :return:  Nothing
        """
        # If there aren't any shouts yet, say so.
        if len(self.shouts) == 0:
            logger.warning("No shouts stored. Try running .fetch_shouts() or there may be a network error.")

        # Otherwise, just print the things.
        else:
            print('\n'.join([str(shout) for shout in self.shouts]))

    def print_updated_shouts(self):
        """
        Print all of the shouts that have been added with the latest fetch.
        :return:
        """
        # If there aren't any shouts yet, say so.
        if self.last_updated == 0:
            logger.warning("No shouts stored. Try running .fetch_shouts() or there may be a network error.")

        # If no shouts have been added with the latest fetch, say so and
        # describe when the last shout was.
        elif self.last_updated == self.last_last_updated:
            print('No new shouts since last shout by {author} at {timestamp}.'.format(
                author=self.shouts[-1].author,
                timestamp=self.shouts[-1].time.strftime('%I:%M:%S %p')
            ))

        # Otherwise, print all of the shouts that are after the last_last_updated
        # flag length.
        else:
            print('\n'.join([str(shout) for shout in self.shouts[self.last_last_updated:]]))

    def get_current_conversation_title(self):
        """
        Gets the title of the conversation on which the headless browser is loaded.
        :return: The conversation title as a string and the id as a string - together in a 2-tuple.
        """
        title, conversation_id = 'No title', -1
        logger.debug(f'Finding conversation title...')

        try:
            # Try to find the current conversation title by its id marker
            element = self.starship.find_elements_by_css_selector(
                '[id^="current-conversation-title-"]'
            )

            # Save the title
            conversation_title = element[0].text
            logger.debug(f'Conversation title: {conversation_title}')

            match = re.match(r'^(.+)-(\d+)$', conversation_title)

            if match:
                title, conversation_id = match.groups()
                logger.debug(f'title, conversation group = {title}, {conversation_id}')

        except IndexError:
            # Raise an error if no conversation title is found.
            logger.error('No conversation title elements found.')

        return title, conversation_id

    def get_current_shout_total(self):
        """
        Extracts the number of shouts that should be found in a conversation from the
        active conversations list.
        :return: An integer corresponding to the expected number of shouts in a conversation
        """
        logger.debug(f'Getting current shout total...')
        conversation_title, conversation_id = self.get_current_conversation_title()
        active_conversation_titles = [conversation for conversation in self.get_conversation_list()]

        for title in active_conversation_titles:
            logger.debug(f'Examining {title}...')
            rematch = re.match(r'^(.+)-(\d+) \((\d+) \^\d+\)$', title)

            if rematch:
                candidate_id, shout_total = rematch.groups()[1], int(rematch.groups()[2])
                logger.debug(f'Candidtate id: {candidate_id}, Shout total: {shout_total}')

                if candidate_id == conversation_id:
                    self.starship.refresh()
                    return shout_total

        raise LookupError(f'Conversation id {conversation_id} not found.')

    def get_conversation_list(self):
        """
        Gets a list of conversations from the klatchat page.
        :return: A list of conversation titles as strings.
        """
        conversations = []
        retry_start = time()

        while (len(conversations) == 0 or conversations[0] == '') and time() - retry_start < 60:
            # Find the conversation navigator expandable and click it.
            self.starship.find_element_by_xpath('//a[@href="#collapseConversationNavigator"]').click()

            # Find the Active Conversation expandable and click it.
            self.starship.find_element_by_xpath('//a[@href="#collapseActiveConvsersations"]').click()

            # Save the updated page html.
            self.raw_html = self.starship.page_source

            # Find all of the elements with the 'join-chat' class.
            logger.debug(f'Got to the place. Now let\'s dig for the conversations...')
            conversations = [conv.text for conv in self.starship.find_elements_by_xpath('//div[@class="join-chat"]')]
            logger.debug(f'conversations found: {conversations}')

        if len(conversations) < 1:
            msg = f"No active conversations found!"
            logger.error(msg)
            raise Exception(msg)

        return conversations

    def get_domain_conversation_list(self):
        logger.debug(f'Getting domain conversation list...')

        try:
            # Find the conversation navigator expandable and click it.
            self.starship.find_element_by_xpath('//a[@href="#collapseConversationNavigator"]').click()

            # Find the domain tab expandable and click it.
            self.starship.find_element_by_xpath('//a[@href="#conversation-select-list-wrap"]').click()

            stale_element = True
            counter = 0
            conversations = None

            # Find all of the elements with the 'profile-conversation' class. Retry until the stale element resolves
            while (stale_element or conversations is None or len(conversations) < 1) and counter < 10:
                try:
                    if conversations is not None and len(conversations) < 1:
                        sleep(1)
                    counter += 1
                    conversations = [conv.text for conv in self.starship.find_elements_by_xpath('//p[@class="profile-conversation"]')]
                    stale_element = False
                    logger.debug(f'Got conversations on count {counter}: {conversations}')

                except StaleElementReferenceException as e:
                    stale_element = True
                    sleep(1)
                    logger.warning(f'Stale Element error. Retrying to get conversations list{" " + str(counter) if counter > 1 else ""}...')

            if conversations is None or len(conversations) < 1:
                raise ValueError('No conversations found in the conversation list!')

            return conversations

        except Exception as e:
            logger.error(f'Error in get_domain_conversation_list: {type(e)} :: {e}')

    def choose_conversation(self):
        """
        Change the conversation the headless browser is loaded to.
        :return:
        """
        # Wait to minimize the probability of errors.
        wait(3)

        # Get the list of active conversations.
        conversation_list = self.get_conversation_list()

        # For each conversation in the list, print an identifier and the title.
        for conversation_id in range(len(conversation_list)):
            print('{id}.  {title}'.format(
                id=conversation_id,
                title=conversation_list[conversation_id]
            ))

        # Ask the user to input the identifier of the conversation to switch to.
        conversation_choice = int(input('Input the id of the conversation you\'d like to switch to:  '))

        # Get the conversation id from the title of the conversation.
        conv_klat_id = re.match(r'.*-(\d{5})', conversation_list[conversation_choice]).group(1)
        
        conv_url = 'http://klat.com/klatchat.php?currConv={}'.format(conv_klat_id)

        # Relaunch the browser to the conversation with the specified id.
        self.launch(url=conv_url)

        # Print confirmation of success.
        logger.info('Switched to conversation "{}"'.format(self.get_current_conversation_title()))
        
        return conv_url

    def close_driver(self):
        """
        Close the headless browser.
        :return: Nothing.
        """
        logger.info(f'Closing the driver...')
        self.starship.quit()
