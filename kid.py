"""
This is the main entry point for the kid. The design is to be able to command `python main ` with arguments to invoke
the different capabilities of the shout kid.
The main framework will import the scripts from the main folder and provide a wrapper that will call each of the
Kid's capable scripts.
"""
import argparse
import sys

import config
from get_stats import *
from leaderboard import get_leaderboard
from make_invoice import make_invoice, combine_invoices
from orbit import orbit, orbit_system
from save_id import save_id
from toybox.utils import set_logger, get_week_name

logger = set_logger('kid')


def orbit_handler(args):
    logger.debug(f'Handling with orbit_handler...')
    if args.id is not None:
        orbit(chat_url=config.KLATCHAT_URL + f'?currConv={args.id}', outfile=args.output)
    elif args.system:
        orbit_system()
    else:
        orbit(outfile=args.output)


def leaderboard_handler(args):
    logger.debug(f'Handling with leaderboard_handler...')
    get_leaderboard(ignore_refresh=args.ignore_refresh, manual_ids=args.manual_ids, manual_id_file=args.id_file,
                    crawl_start=args.crawl_start, max_threads=args.max_threads, output_file=args.output)


def make_invoice_handler(args):
    logger.debug(f'Handling with make_invoice_handler...')
    if args.combine is None and args.extra_charge is None:
        make_invoice(file=args.file, address=args.address, usernames=args.usernames,
                     limit=args.limit, earning=args.earning, outfile=args.output)
    elif args.extra_charge is not None:
        make_invoice(file=args.file, address=args.address, usernames=args.usernames,
                     limit=args.limit, earning=args.earning, extra_charges=args.extra_charge, outfile=args.output)
    elif len(args.combine) == 1:
        combine_invoices(weeks_back_start=args.combine[0])
    elif len(args.combine) > 1:
        combine_invoices(weeks_back_start=args.combine[0],
                         weeks_back_end=args.combine[1])


def save_id_handler(args):
    logger.debug(f'Handling with save_id_handler...')
    save_id(file=args.output)


def stats_handler(args):
    logger.debug(f'Handling with stats_handler...')

    if args.files is not None:
        for file_name in args.files:
            logger.debug(f'Checking file {file_name}...')
            if file_name == 'this-week':
                file_name = get_week_name() + '.klat'
                args.files.append(file_name)
                args.files.remove('this-week')
                logger.debug(f'Translated this-week to {file_name}')
                continue
            elif file_name == 'last-week':
                file_name = get_week_name(weeks_back=1) + '.klat'
                args.files.append(file_name)
                args.files.remove('last-week')
                logger.debug(f'Translated last-week to {file_name}')
                continue

            if file_name is not None and \
                    not os.path.exists(os.path.join(config.LOG_FILE_PATH, file_name)):
                logger.error(f'File "{file_name}" does not exist!')
                sys.exit(1)

    if args.type == 'authors':
        logger.info(f'Getting the distribution of shouts by author...')
        dist = get_authors(args.files)
        logger.info(f'Distribution by author:\n{dist}')
    elif args.type == 'hours':
        logger.info(f'Getting the distribution of shouts over hours in the week...')
        dist = get_hour_weekday_table(args.files, args.author)
        logger.info(f'Distribution by hour in the week for {args.author}:\n{dist}')
    elif args.type == 'words':
        logger.info(f'Getting the distribution of words in the shouts from {args.author}...')
        if args.word is None:
            dist = get_word_frequencies(args.files, args.author, min_freq=args.min_freq, topn=args.topn)
        else:
            logger.info(f'...for the word "{args.word}"')
            dist = get_word_frequencies(args.files, args.author, args.word)
        logger.info(f'Distribution for {args.author}:\n{dist}')
    elif args.type == 'description':
        logger.info(f'Getting the description of shouts for {args.author}...')
        dist = describe_shouts(args.files, args.author)
        logger.info(f'Shout description for {args.author}:\n{dist}')
    else:
        logger.error(f'"{args.type}" is not a valid stat type to get.\n'
                     f'Must be one of "authors", "hours", "words", or "description".')


if __name__ == '__main__':
    logger.debug(f'Starting the KID...')

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    orbit_parser = subparsers.add_parser('orbit', help='Watch a conversation and be notified when someone shouts.')
    invoice_parser = subparsers.add_parser('invoice', help='Compose an invoice. If optional arguments are omitted,'
                                                           'the KID will ask the user for the necessary information.')
    totals_parser = subparsers.add_parser('totals', help='Show all of the shout counts by username. With no arguments,'
                                                         'the KID will ask if you want to record the shouts from '
                                                         'conversations in the current domain.')
    save_id_parser = subparsers.add_parser('save-id', help='Save the current conversation\'s id.')
    stats_parser = subparsers.add_parser('get-stats', help='Get statistics about saved shouts.')

    # Orbit options
    orbit_parser.add_argument('-i', '--id')
    orbit_parser.add_argument('-s', '--system', action='store_true',
                              help='Watch the conversations, perhaps to watch for a specific one to be created.')
    orbit_parser.add_argument('-o', '--output', default='this-week',
                              help='File to which to save the shouts and/or ids.')
    orbit_parser.set_defaults(func=orbit_handler)

    # make invoice options
    totals_group = totals_parser.add_mutually_exclusive_group()
    totals_group.add_argument('--no-refresh', action='store_true', dest='ignore_refresh',
                              help='Show the shout counts loaded directly from file without refreshing the '
                                   'conversations.')
    totals_group.add_argument('--ids', nargs='*', dest='manual_ids',
                              help='Input conversation ids which correspond to the conversations you would like to '
                                   'save.')
    totals_group.add_argument('-i', '--id-file', dest='id_file',
                              help='Input a filename in which conversation ids are saved which correspond to the '
                                   'conversations you would like to save.')
    totals_group.add_argument('-s', '--start', type=int, dest='crawl_start',
                              help='Specify the conversation id at which to start crawling conversations. '
                                   'Goes through each id individually instead of jumping to each conversation in the '
                                   'domain as is default.')
    totals_parser.add_argument('-o', '--output',
                               help='File to which to save the shouts and/or ids.')
    totals_parser.add_argument('-p', '--processes', dest='max_threads', default=None, type=int,
                               help='The maximum number of processes to use when getting the '
                                    'shouts from conversations.')
    totals_parser.set_defaults(func=leaderboard_handler)

    # Make invoice options
    invoice_parser.add_argument('-f', '--file',
                                help='File name to read information from to make the invoice.')
    invoice_parser.add_argument('-a', '--address',
                                help='PayPal email address to use in the constructed invoice.')
    invoice_parser.add_argument('-u', '--usernames', nargs='+',
                                help='Set the usernames used in counting shout totals for and listing in the invoice.')
    invoice_parser.add_argument('-l', '--limit', type=int,
                                help='Override the upper limit for an invoice.')
    invoice_parser.add_argument('-e', '--earning', type=int,
                                help='Override the cents per shout when calculating the earnings.')
    invoice_parser.add_argument('-o', '--output',
                                help='File to which to save the shouts and/or ids.')
    invoice_group = invoice_parser.add_mutually_exclusive_group()
    invoice_group.add_argument('-m', '--misc-charge', nargs='+', dest='extra_charge',
                               help='Add one or more miscellaneous charges in AMOUNT, DESCRIPTION pairs.\n'
                                    'For example: \'5 "bug report" 10 "other bug report"\'')
    invoice_group.add_argument('--combine', nargs='+', type=int,
                               help='Combine the past n invoices until m invoices back. m is an optional argument.')
    invoice_parser.set_defaults(func=make_invoice_handler)

    # Save ID parser arguments
    save_id_parser.set_defaults(func=save_id_handler)
    save_id_parser.add_argument('-o', '--output',
                               help='File to which to save the shouts and/or ids.')

    # Get stats parser arguments
    stats_parser.add_argument('type',
                              help='The statistics to get.\n'
                                   'Must be one of "authors", "hours", "words", or "description".')
    stats_parser.add_argument('-f', '--files', nargs='+', default=None,
                              help='List the names of the files to load for the statistics.')
    stats_parser.add_argument('-a', '--author', default='everyone',
                              help='Filter the stats with an author.')
    stats_group = stats_parser.add_mutually_exclusive_group()
    stats_group.add_argument('-w', '--word',
                             help='Word to look for if stats type is "words".')
    stats_group.add_argument('-m', '--min-freq', dest='min_freq', type=int, default=0,
                             help='Give a minimum frequency of a word occurring to return in the word frequency table.')
    stats_group.add_argument('-t', '--topn', type=int, default=None,
                             help='Give a maximum number of records to return in the word frequency table.')

    stats_parser.set_defaults(func=stats_handler)

    # For crying out loud, actually parse the args!
    args = parser.parse_args()
    logger.debug(f'Set up the parser with args: {args}')

    logger.debug(f'Executing...')
    try:
        args.func(args)

    except (Exception, SyntaxError) as e:
        logger.critical(f'Uncaught {type(e).__name__} error: {e}')
