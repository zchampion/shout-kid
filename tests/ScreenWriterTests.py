import unittest
from datetime import datetime

from toybox.ScreenWriter import ScreenWriter
from toybox.shout import Shout


class ScreenWriterTest(unittest.TestCase):
    def setUp(self):
        self.scw = ScreenWriter()

    def test_get_week_name_now(self):
        week_name = self.scw.get_week_name(weeks_back=0)
        approval = input('Is {month1}/{day1}/{year} - {month2}/{day2}/{year} this week? [y/n]  '.format(
            year=week_name[:2],
            month1=week_name[2:4],
            day1=week_name[4:6],
            month2=week_name[6:8],
            day2=week_name[8:]
        )).lower()[:1]
        self.assertEqual(approval, 'y')

    def test_get_last_week_name(self):
        week_name = self.scw.get_week_name(weeks_back=1)
        approval = input('Is {month1}/{day1}/{year} - {month2}/{day2}/{year} last week? [y/n]  '.format(
            year=week_name[:2],
            month1=week_name[2:4],
            day1=week_name[4:6],
            month2=week_name[6:8],
            day2=week_name[8:]
        )).lower()[:1]
        self.assertEqual(approval, 'y')

    def test_read_shouts(self):
        shouts_read = self.scw.read_shouts(filename='test-data-1.txt')
        self.assertEqual(type(shouts_read), list)
        self.assertEqual(type(shouts_read[0]), Shout)
        self.assertGreater(len(shouts_read), 0)

    def test_write_shouts(self):
        self.scw.shout_buffer = [Shout('2020-4-26 3:14:16AM', 'TestShouter', 'Test message!', None, None, None)]
        self.scw.write_shouts()
        self.assertGreater(len(self.scw.read_shouts()), 0)
