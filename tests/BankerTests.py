import unittest

from toybox.shout import Shout
from toybox.Cowboy import Cowboy
from toybox.Banker import Banker


class BankerTest(unittest.TestCase):
    def setUp(self):
        self.bnk = Banker('paypalemail@email.com', usernames=['TheFluffyQ'], shout_count=6)

    def test_optional_args(self):
        self.bnk = Banker('paypalemail@email.com')

    def test_birth(self):
        self.assertIsNotNone(self.bnk)

    def test_attributes(self):
        self.assertEqual(10, self.bnk.cents_per_shout)
        self.assertEqual(50, self.bnk.maximum_invoice_amount)

    def test_calculate_shout_earnings(self):
        self.assertEqual(10, self.bnk.cents_per_shout)
        self.assertEqual(6, self.bnk.shout_count)
        self.assertEqual(0.6, self.bnk.calculate_shout_earnings())

    def test_calculate_total_earnings(self):
        self.assertEqual(10, self.bnk.cents_per_shout)
        self.assertEqual(6, self.bnk.shout_count)
        self.assertEqual(0.6, self.bnk.calculate_total_earnings())

    def test_over_max_shouts(self):
        self.bnk.shout_count = int(11.5 * self.bnk.maximum_invoice_amount)
        self.assertEqual(self.bnk.maximum_invoice_amount, self.bnk.calculate_total_earnings())

    def test_compiling_invoice(self):
        print(self.bnk.compose_invoice())
        self.bnk.usernames.add('guest_lordfarquaad')
        print(self.bnk.compose_invoice())
