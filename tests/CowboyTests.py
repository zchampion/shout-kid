import unittest

from toybox.Astronaut import Astronaut
from toybox.Cowboy import Cowboy
from toybox.utils import wait


class CowboyTest(unittest.TestCase):
    def setUp(self):
        self.ast = Astronaut()
        print('Launching into klat space...')
        self.ast.launch()
        print(f'Testing with conversation "{self.ast.get_current_conversation_title()}"...')
        self.ast.fetch_conversation()
        self.cwb = Cowboy()

    def test_new_cowboy(self):
        self.assertIsNotNone(self.cwb)

    def test_load_shouts(self):
        self.ast.fetch_shouts()
        prev_shout_num = len(self.cwb.raw_shouts)

        while not self.ast.last_updated > self.ast.last_last_updated:
            print('Testing loading new shouts into the Cowboy, but '
                  'there\'s no new shouts yet.')
            wait(30)
            self.ast.fetch_shouts()

        self.assertGreater(len(self.cwb.raw_shouts), prev_shout_num)

