import unittest

from time import sleep

from toybox.Astronaut import Astronaut


class AstronautTest(unittest.TestCase):
    def setUp(self):
        self.ast = Astronaut()
        self.ast.launch()

    def test_squirrel_birth(self):
        self.assertIsNotNone(self.ast)

    def test_fetch_shouts(self):
        self.ast.fetch_shouts()
        self.assertGreater(len(self.ast.shouts), 0)

    def test_fetch_conversation(self):
        self.ast.fetch_conversation()
        self.assertGreater(len(self.ast.shouts), 0)

    def test_string_for_html(self):
        self.assertRegex(self.ast.raw_html, '<.*>.*</.*>')

    def test_for_shout_presence(self):
        self.assertRegex(self.ast.raw_html, "<span class=\"shout-bubble ")

    def test_get_current_conversation_title(self):
        title = self.ast.get_current_conversation_title()
        self.assertEqual(type(title), tuple)
        self.assertEqual(len(title), 2)
        self.assertNotEqual(title, ('No title', -1))

    def test_get_current_shout_total(self):
        for _ in range(10):
            self.assertGreater(self.ast.get_current_shout_total(), 0)
            sleep(1)

    def tearDown(self):
        self.ast.close_driver()
