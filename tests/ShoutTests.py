import unittest
from random import randint, shuffle, choice
from string import ascii_letters

from toybox.shout import Shout


class TestShout(unittest.TestCase):
    def setUp(self):
        self.test_shouts = [
            "Wed 8:34pm: KatharineSwan I'd believe that, he's kind of a tool.",
            "Wed 8:23pm: Holly (1) they are the 800 lb gorilla",
            "Wed 8:22pm: KatharineSwan 1) Employee issues.  I've heard so " +
            "many horror stories about how poorly they treat their employees.",
            "Wed 0:50am: adann62 welp i guess it's super dead today"
        ]
        self.test_Shout_1 = Shout(self.test_shouts[0])

    def test_normal_shout(self):
        shout = Shout(self.test_shouts[0])
        self.assertEquals(str(shout),
                          'On Wednesday at 08:34PM, KatharineSwan said, ' +
                          '"I\'d believe that, he\'s kind of a tool."')

    # Feature deferred to a future ticket #
    # def test_space_in_username(self):
    #     shout = Shout(self.test_shouts[1])
    #     self.assertEquals(str(shout),
    #                       'On Wednesday at 08:23PM, Holly (1) said, ' +
    #                       '"they are the 800 lb gorilla"')

    def test_long_shout(self):
        shout = Shout(self.test_shouts[2])
        self.assertEquals(str(shout),
                          'On Wednesday at 08:22PM, KatharineSwan said, ' +
                          '"1) Employee issues.  I\'ve heard so ' +
                          'many horror stories about how poorly they treat ' +
                          'their employees."')

    def test_invalid_date(self):
        shout = Shout('Wrong date: Author The message is me.')

    def test_invalid_author_message_combo(self):
        shout = Shout('Wed 8:34pm: Therearenospaceshere')

    def test_random_invalid_shouts(self):
        invalid_list = list(ascii_letters + ':               ')
        shuffle(invalid_list)
        invalid_string = ''.join(invalid_list)[:randint(1, len(invalid_list))]

        print("Randomly generated string: " + invalid_string)

        shout = Shout(invalid_string)

    def test_datetime_parse(self):
        test_date_string = '{day} {hour}:{min:02d}{daypart}'.format(
            day=choice(['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']),
            hour=randint(0, 12),
            min=randint(0,59),
            daypart=choice(['am', 'pm'])
        )

        print("Randomly generated date: " + test_date_string)
        print("Date parsed: " + self.test_Shout_1.parse_time_string(test_date_string).isoformat())
