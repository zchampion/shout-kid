"""
Shout KID
    Copyright (C) 2019  Zac Champion  zchampion270@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
from datetime import datetime

DRIVER_DIR_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               'geckodriver')
KLATCHAT_URL = 'https://klat.com/klatchat.php'
TIMESTAMP_FMT = '%I:%M:%S'
CENTS_PER_SHOUT = 10
MAXIMUM_INVOICE_AMOUNT = 50

LOG_FILE_PATH = 'logs'
INVOICE_FILE_PATH = 'invoices'

KID_LOGGING_FILE_PATH = 'kid_logs'
KID_LOGGING_FILE_NAME = datetime.today().strftime('%y.%m.%d') + '.log'

DRIVER_BIN_PATH = ''
os_name = os.uname().sysname

if os_name == 'Windows':
    DRIVER_BIN_PATH = 'geckodriver.exe'

elif os_name == 'Darwin':
    DRIVER_BIN_PATH = 'geckodriver_mac'

elif os_name == 'Linux':
    DRIVER_BIN_PATH = 'geckodriver_linux'

else:
    print(f'Well, crap. This is the real os.uname().sysname: "{os_name}"')

DRIVER_PATH = os.path.join(DRIVER_DIR_PATH, DRIVER_BIN_PATH)

COMMON_WORDS = [w.upper() for w in [
    'the',
    'be',
    'to',
    'of',
    'and',
    'a',
    'in',
    'that',
    'have',
    'I',
    'it',
    'for',
    'not',
    'on',
    'with',
    'he',
    'as',
    'you',
    'do',
    'at',
    'this',
    'but',
    'his',
    'by',
    'from',
    'they',
    'we',
    'say',
    'her',
    'she',
    'or',
    'an',
    'will',
    'my',
    'one',
    'all',
    'would',
    'there',
    'their',
    'what',
    'so',
    'up',
    'out',
    'if',
    'about',
    'who',
    'get',
    'which',
    'go',
    'me',
    'when',
    'make',
    'can',
    'like',
    'time',
    'no',
    'just',
    'him',
    'know',
    'take',
    'people',
    'into',
    'year',
    'your',
    'good',
    'some',
    'could',
    'them',
    'see',
    'other',
    'than',
    'then',
    'now',
    'look',
    'only',
    'come',
    'its',
    'over',
    'think',
    'also',
    'back',
    'after',
    'use',
    'two',
    'how',
    'our',
    'work',
    'first',
    'well',
    'way',
    'even',
    'new',
    'want',
    'because',
    'any',
    'these',
    'give',
    'day',
    'most',
    'us'
]]
