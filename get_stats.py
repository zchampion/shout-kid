import os

import config
import toybox


logger = toybox.utils.set_logger('get_stats')


def _get_all_file_names():
    """
    Get the file names of all of the log files available with shout data.
    :return:
    """
    logger.debug(f'Getting all file names available in dir {config.LOG_FILE_PATH}')
    file_names = os.listdir(config.LOG_FILE_PATH)
    if '.keep' in file_names:
        file_names.remove('.keep')
    logger.debug(f'Got file names: {", ".join(file_names)}')
    return file_names


def _process_filenames(files):
    """
    Helper function to process the files parameter common to many get_stats functions
    :param files:
    :return:
    """
    if files is None:
        files = _get_all_file_names()
    elif type(files) != list:
        files = [files]
    elif type(files) == str:
        files = [files]
    return files


def _get_data(files):
    """
    Get all of the shout data and load it into a Cowboy object.
    :return: A cowboy object available for testing.
    """
    logger.info(f'Getting the shout data from {", ".join(files)}')

    sw = toybox.ScreenWriter()
    cb = toybox.Cowboy()
    shouts = []

    for file in files:
        shouts += sw.read_shouts(file)

    cb.load_shouts(shouts)

    logger.debug(f'Got data. DF shape: {cb.shout_df.shape}')

    return cb


def get_authors(files=None):
    """
    Gets the distribution of authors of shouts over the shouts from the files given.
    :param files:
    :return:
    """
    files = _process_filenames(files)

    cb = _get_data(files)

    authors = cb.get_author_distribution()

    return authors


def get_hour_weekday_table(files=None, author='everyone'):
    """
    Gets the distribution of shouts by weekday and hour of the day
    Weekdays are in number format, 0=Monday through 6=Sunday
    """
    files = _process_filenames(files)

    cb = _get_data(files)

    hours = cb.get_hour_distribution(author)

    return hours


def get_word_frequencies(files=None, author='everyone', word=None, min_freq=0, topn=None):
    """
    Gets the words used in the shouts by how often they're used.
    Can also filter by author.
    :param topn:
    :param min_freq:
    :param word:
    :param files: Files to load for the data.
    :param author: Author to filter by.
    :return:
    """
    files = _process_filenames(files)

    cb = _get_data(files)

    meaningful_words, common_words = cb.get_summary_of_words(author, min_freq, topn)

    if word is not None:
        word_row = cb.get_word_rank(word, author=author, word_ranks=(meaningful_words, common_words))
        return word_row

    else:
        return meaningful_words


def describe_shouts(files=None, author='everyone'):
    """
    Gets a table that describes the distribution of the number of words and characters in someone's shouts.
    :param files:
    :param author:
    :return:
    """
    files = _process_filenames(files)

    cb = _get_data(files)

    description_table = cb.describe_shouts_of(author)

    return description_table
